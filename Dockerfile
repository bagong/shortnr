FROM tiangolo/uvicorn-gunicorn-fastapi:python3.8-slim

ENV VARIABLE_NAME=APP

COPY requirements.txt /tmp/requirements.txt
RUN pip install --no-cache-dir -r /tmp/requirements.txt

# TODO: run as non-root user, also change to non-privileged port

COPY ./shortnr /app/app

