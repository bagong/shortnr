## What This Is
This is a simple backend application for an URL shortener service, built using
[FastAPI](https://fastapi.tiangolo.com) and prepared for deployment using docker
container.

The `docker-compose` orchestrator is configured to run the application on port
80.

## Requirements
- docker
- docker-compose

## Usage
1. Make sure `docker` and `docker-compose` installed on your test machine/VM
   (see "Test Environment") below
2. Clone this repository
3. Build container: `docker-compose build`
4. Run container: `docker-compose up` (or add flag `-d` to run in background)
5. Open API Documentation at following address to play with the API:
- `http://<YOUR_TEST_MACHINE_IP>/docs`

Both API Endpoints will be available at:
- `http://<YOUR_TEST_MACHINE_IP>/encode`
- `http://<YOUR_TEST_MACHINE_IP>/decode`


### Testing
```bash
pip install tox
tox
```


## Test Environment
### Setup
```bash
# On fresh installed Ubuntu 20.04 server
sudo apt install docker.io  # Debian upstream package is now up-to-date
sudo apt install python3-pip
sudo pip3 install docker-compose
sudo usermod -aG docker $(whoami)
newgrp docker
```

### Versions
```
$ docker --version
Docker version 20.10.6, build 370c289

$ docker-compose --version
docker-compose version 1.28.6, build unknown

$ uname -r
5.8.0-53-generic

$ cat /etc/lsb-release
DISTRIB_ID=Ubuntu
DISTRIB_RELEASE=20.04
DISTRIB_CODENAME=focal
DISTRIB_DESCRIPTION="Ubuntu 20.04.2 LTS"
```

