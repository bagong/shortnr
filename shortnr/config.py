import os

from pydantic import BaseSettings


class Settings(BaseSettings):
    hasher_salt: str = os.getenv("HASHER_SALT", "my salt is the saltiest")
    hasher_min_length: int = int(os.getenv("HASHER_MIN_LENGTH", "4"))


# Simple global variable for simple program.
# To be able to adjust settings on the fly, check reference below:
# https://fastapi.tiangolo.com/advanced/settings/#settings-in-a-dependency
settings = Settings()
