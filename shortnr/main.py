import logging
from typing import List, Tuple

import uvicorn
import validators
from fastapi import FastAPI, HTTPException, Request, status
from hashids import Hashids
from pydantic import BaseModel
from starlette.responses import RedirectResponse

from .config import settings


class URLIDInvalidException(Exception):
    pass


class LongShortURL(BaseModel):
    original_url: str
    short_url: str
    url_id: str


logging.basicConfig(level=logging.DEBUG)
LOGGER = logging.getLogger(__name__)

APP = FastAPI()

HASHER = Hashids(salt=settings.hasher_salt, min_length=settings.hasher_min_length)

# Simple list to hold the list or URLs in memory
ORIGINAL_URLS: List[str] = []


@APP.post("/encode", response_model=LongShortURL, status_code=status.HTTP_201_CREATED)
def encode(original_url: str, request: Request) -> LongShortURL:
    valid_url = validators.url(original_url)
    if not valid_url:
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail="Given URL is invalid!",
        )

    # Prevent malicious DDoS attempt of recurrent redirection
    if original_url.startswith(str(request.base_url)):
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail="Can not shorten short URL",
        )

    ORIGINAL_URLS.append(original_url)
    url_id: str = HASHER.encode(len(ORIGINAL_URLS) - 1)

    base_url = str(request.base_url)
    short_url = base_url + url_id

    LOGGER.debug(
        f"len: {len(ORIGINAL_URLS)}, latest URL_ID: {url_id} URL: {original_url}"
    )

    return LongShortURL(original_url=original_url, short_url=short_url, url_id=url_id)


def retrieve_original_url(url_id: str) -> str:
    idxs: Tuple[int] = HASHER.decode(url_id)
    if len(idxs) > 0:
        idx = idxs[0]
        if -1 < idx < len(ORIGINAL_URLS):
            original_url: str = ORIGINAL_URLS[idx]
        else:
            raise URLIDInvalidException
    else:
        raise URLIDInvalidException

    return original_url


@APP.get("/decode/{url_id}")
def decode(url_id: str, request: Request) -> LongShortURL:
    try:
        original_url: str = retrieve_original_url(url_id)
    except URLIDInvalidException as url_invalid_ex:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Invalid Hash ID"
        ) from url_invalid_ex

    base_url = str(request.base_url)
    short_url = base_url + url_id

    return LongShortURL(original_url=original_url, short_url=short_url, url_id=url_id)


@APP.get("/{url_id}")
def redirect(url_id: str) -> RedirectResponse:
    try:
        original_url: str = retrieve_original_url(url_id)
    except URLIDInvalidException as url_invalid_ex:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Invalid Hash ID"
        ) from url_invalid_ex

    resp = RedirectResponse(original_url)
    return resp


if __name__ == "__main__":
    # This shall only be executed on IDEs for debugging and development purposes,
    # production instance will be handled by uvicorn from shell
    uvicorn.run("main:APP", host="0.0.0.0", port=8000, reload=True, debug=True)
