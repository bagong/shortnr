import random

import pytest
from faker import Faker
from fastapi.testclient import TestClient
from hashids import Hashids

from shortnr.main import APP

SALT = "my salt is the saltiest"

Faker.seed(1337)


@pytest.fixture
def client():
    c = TestClient(APP, base_url="http://sho.rt")
    return c


def test_read_root(client):
    response = client.get("/")
    assert response.status_code == 404


def test_invalid_url_ids(client):
    # build server database
    faker = Faker()
    for idx in range(100):
        original_url = faker.uri()
        resp = client.post(f"/encode?original_url={original_url}")
        assert resp.status_code == 201

    # check decoding invalid URL IDs
    # Hash collision is more likely to happen with short hash length
    h = Hashids(salt="for invalid ids test", min_length=4)
    for i in range(100):
        url_id = h.encode(i)
        resp = client.get(f"/decode/{url_id}")
        assert resp.status_code == 404


def test_normal_cases(client):
    faker = Faker()
    long_short_url_pairs = []

    # build server database first
    for idx in range(100):
        original_url = faker.uri()
        resp = client.post(f"/encode?original_url={original_url}")

        assert resp.status_code == 201
        assert resp.json()["original_url"] == original_url

        short_url = resp.json()["short_url"]
        url_id = resp.json()["url_id"]
        long_short_url_pairs.append((original_url, short_url, url_id))

    # check if server returns correct long URLs
    for original_url, short_url, url_id in long_short_url_pairs:
        resp = client.get(f"/decode/{url_id}")
        assert resp.status_code == 200
        assert resp.json()["original_url"] == original_url


def test_invalid_original_urls(client):
    faker = Faker()
    unsafe_chars = [" ", "|", "[", "}"]
    len_unsafe_chars = len(unsafe_chars)

    for idx in range(50):
        original_url = faker.uri()
        invalid_original_url = list(original_url)
        invalid_original_url[random.randint(0, len(original_url) - 1)] = unsafe_chars[
            random.randint(0, len_unsafe_chars - 1)
        ]
        invalid_original_url = "".join(invalid_original_url)
        resp = client.post(f"/encode?original_url={invalid_original_url}")

        assert resp.status_code == 422


def test_recurrent_redirection(client):
    faker = Faker()

    original_url = faker.uri()
    resp = client.post(f"/encode?original_url={original_url}")
    assert resp.status_code == 201

    short_url = resp.json()["short_url"]
    print(short_url)
    resp = client.post(f"/encode?original_url={short_url}")
    print(resp.json())
    assert resp.status_code == 422


def test_valid_redirections(client):
    # build server database
    url_pairs = []

    faker = Faker()
    for idx in range(10):
        original_url = faker.uri()
        resp = client.post(f"/encode?original_url={original_url}")
        assert resp.status_code == 201
        url_id = resp.json()["url_id"]
        url_pairs.append([original_url, url_id])

    for original_url, url_id in url_pairs:
        resp = client.get(f"/{url_id}")
        # FIXME: why 404 arrives here instead of 307???
        # assert resp.status_code == 307
        assert resp.url in original_url


def test_nonexistent_redirections(client):
    # check decoding invalid URL IDs
    # Hash collision is more likely to happen with short hash length
    h = Hashids(salt="for invalid ids test", min_length=4)
    for i in range(50):
        url_id = h.encode(i)
        resp = client.get(f"/{url_id}")
        assert resp.status_code == 404
